from __future__ import absolute_import, division, print_function, unicode_literals

import os

import params

import numpy as np
from keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
import pandas as pd
from keras import Sequential
from keras.layers import Embedding, SpatialDropout1D, LSTM, Dense, Bidirectional
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
from sklearn.utils import shuffle
import pickle
import keras
import matplotlib.pyplot as plt

from parser import clean_text

C = params.Config()
DEFAULT_MODEL_NAME = 'multi'


def create_model(data, activation_function="sigmoid"):
    model = Sequential()
    model.add(Embedding(C.MAX_NB_WORDS, C.EMBEDDING_DIM, input_length=data.shape[1]))
    model.add(Bidirectional(LSTM(100, return_sequences=True), input_shape=(5, 10)))
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(4, activation=activation_function))
    opt = keras.optimizers.Adam(learning_rate=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    print("Using activation function", activation_function)

    return model


def tokenize_data(df):
    tokenizer = Tokenizer(num_words=C.MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
    df = shuffle(df)
    tokenizer.fit_on_texts(df['productDescription'].values)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    X = tokenizer.texts_to_sequences(df['productDescription'].values)
    X = pad_sequences(X, maxlen=C.MAX_SEQUENCE_LENGTH)
    print('Shape of data tensor:', X.shape)
    Y = pd.get_dummies(df['code']).values
    print('Shape of label tensor:', Y.shape)

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.10, random_state=42)
    print(X_train.shape, Y_train.shape)
    print(X_test.shape, Y_test.shape)

    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return X, X_train, Y_train, X_test, Y_test


def predict(label, text, model_obj=None):
    text = clean_text(text)

    with open('tokenizer.pickle', 'rb') as handle:
        tokenizer_obj = pickle.load(handle)

    assert (model_obj is not None)
    assert (tokenizer_obj is not None)

    word_index = tokenizer_obj.word_index
    print('Found %s unique tokens.' % len(word_index))
    seq = tokenizer_obj.texts_to_sequences([text])
    print(seq)
    padded = pad_sequences(seq, maxlen=C.MAX_SEQUENCE_LENGTH)
    print(padded)
    pred = model_obj.predict(padded)
    print(pred)

    top_prediction_index = np.argmax(pred)
    predicted_label = extract_label(top_prediction_index)
    predictions = pred.tolist()[0]
    extracted_predictions = [{extract_label(i):"%.2f%%"%(x*100)} for i, x in enumerate(predictions)]
    top_percent = "%.2f%%"% (predictions[top_prediction_index] * 100)
    print(f"{label}\t{text}\t\t{top_percent} {predicted_label}")
    return extracted_predictions


def extract_label(index):
    """
        The labels correspond to exact label indices, in other words, the
        order is crucial.
    """
    return C.labels[index]


def check_activations(path, data, X_train, Y_train, X_test, Y_test):
    best_accuracy = 0
    best_act = ''
    for activation in C.activations:
        if os.path.isfile(path + '/weights.hdf5'):
            os.remove(path + '/weights.hdf5')
        print("Checking", activation)
        accr = run_training(activation, path, data, X_train, Y_train, X_test, Y_test)

        if accr[1] > best_accuracy:
            print("New highscore!")
            best_act = activation
            best_accuracy = accr[1]

    print("Best accuracy is for", best_act)
    return best_act


def run_training(activation, path, data, X_train, Y_train, X_test, Y_test):
    with open("config.pickle", 'wb') as config_f:
        pickle.dump(C, config_f)
        print('Config has been written to {}, and can be loaded when testing to ensure correct results'.format(
            "config.pickle"))

    model = create_model(data, activation)

    training = model.fit(X_train, Y_train, epochs=C.EPOCHS_PARAM, batch_size=C.BATCH_PARAM,
                        validation_split=int(C.validation_split),
                        callbacks=[EarlyStopping(monitor='loss', patience=3, min_delta=0.0001)])
    print("Test set check:")
    accr = model.evaluate(X_test, Y_test)
    print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0], accr[1]))

    # model.save_weights(path + '/weights.hdf5')
    model.save(path + '/weights.hdf5')

    # history_dict = training.history
    # print(history_dict.keys())
    # accuracy = history_dict.history['acc']
    # val_accuracy = history_dict.history['val_acc']
    # loss = history_dict.history['loss']
    # val_loss = history_dict.history['val_loss']
    # epochs = range(1, len(accuracy) + 1)
    #
    # plt.plot(epochs, accuracy, 'bo', label='Training accuracy')
    # plt.plot(epochs, val_accuracy, 'b', label='Validation accuracy')
    # plt.title('Training and validation accuracy')
    # plt.legend()
    #
    # plt.figure()
    #
    # plt.plot(epochs, loss, 'bo', label='Training loss')
    # plt.plot(epochs, val_loss, 'b', label='Validation loss')
    # plt.title('Training and validation loss')
    # plt.legend()
    #
    # plt.show()

    return accr

