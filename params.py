class Config:

    def __init__(self):

        # Train param
        self.validation_split=0.1
        # The maximum number of words to be used. (most frequent)
        self.MAX_NB_WORDS = 50000
        # Max number of words in each description.
        self.MAX_SEQUENCE_LENGTH = 100
        # This is fixed.
        self.EMBEDDING_DIM = 100
        # Handling type labels
        self.labels = ['SM', 'BU', 'BG', '2M']
        # Different types of activation functions
        self.activations = ["relu", "selu", "sigmoid", "softmax", "softplus", "softsign", "tanh", "exponential"]

        self.EPOCHS_PARAM = 20
        self.BATCH_PARAM = 128
