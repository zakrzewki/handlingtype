import re
import unicodedata

import pandas as pd

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
REMOVE = re.compile('!\"#\$%&()\*\+,-./:;<=>?@^_`{|}~')


def parse(input_path):
    """
        :param input_path: String with path containing data for training
        :return: Pandas data frame with sku, description and handling type code
    """
    if not input_path:
        raise FileNotFoundError('There is no input path passed')

    df = pd.read_csv(input_path, index_col=0, sep=";").dropna()
    df['productDescription'] = df['productDescription'].str.replace(',', '')
    df.drop_duplicates(subset="productDescription", keep="first")
    df['productDescription'] = df['productDescription'].apply(clean_text)

    return df


def clean_text(text):
    """
        text: a string
        return: modified initial string
    """
    text = str(unicodedata.normalize('NFD', text).encode('ASCII', 'ignore'))
    text = text.lower()  # lowercase text
    text = text.replace('<br>', ' ')
    text = text.replace('<br/>', ' ')
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub('', text)
    text = REMOVE.sub('', text)
    text = text.replace('x', '')
    text = text.replace('\d+', ' ')
    text = text.replace('    ', ' ')
    text = text.replace('   ', ' ')
    return text


def text_to_df(text):
    data = [text]

    df = pd.DataFrame(data, columns=['productDescription'])
    df['productDescription'] = df['productDescription'].str.replace(',', '')
    df['productDescription'] = df['productDescription'].apply(clean_text)

    return df


def csv_to_df(input_path):

    df = pd.read_csv(input_path, index_col=0, sep=";").dropna()
    df['productDescription'] = df['productDescription'].str.replace(',', '')
    df.drop_duplicates(subset="productDescription", keep="first")
    df['productDescription'] = df['productDescription'].apply(clean_text)

    return df
