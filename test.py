import numpy as np
from keras.models import load_model
from optparse import OptionParser
from sys import platform
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
import helper
import pickle
import params
from parser import parse, text_to_df, csv_to_df

C = params.Config()
path = ''
if platform == "linux" or platform == "linux2":
    path = str("/")
elif platform == "darwin":
    path = str("./")
elif platform == "win32":
    path = str("../")

parser = OptionParser()

parser.add_option("-t", "--type", dest="test_type",
                  help="Choose a type of the prediction - 1 for file, 2 for sentence", default=1)
parser.add_option("-p", "--path", dest="test_path",
                  help="Path with descriptions to predict.")
parser.add_option("-d", "--description", dest="desc",
                  help="Pass a sentence to predict")
parser.add_option("-w", "--weights", dest="weights_path",
                  help="Path with trained weights.", default=path + "/weights.hdf5")
parser.add_option("--config_filename", dest="config_filename",
                  help="Location to read the metadata related to the training (generated when training).",
                  default="config.pickle")
parser.add_option("--token_filename", dest="token_filename",
                  help="Location to read the tokenizer object.", default="tokenizer.pickle")

(options, args) = parser.parse_args()
all_data = None
if options.test_type is "1":
    if not options.test_path:
        parser.error('Path to test data must be specified. Pass --path to command line or select type 2 (only text)')
    all_data = csv_to_df(path + options.test_path)
elif options.test_type is "2":
    if not options.desc:
        parser.error('Description must be specified. Pass --description to command line or select type 1 (csv)')
    all_data = text_to_df(options.desc)
else:
    parser.error('Mode not specified')

# import weights
models_dir = options.weights_path
model = load_model(models_dir)

# import pickle
config_output_filename = options.config_filename
with open(config_output_filename, 'rb') as f_in:
    C = pickle.load(f_in)

for i in range(len(all_data.index)):
    txt = all_data.iloc[i]['productDescription']
    label = str(all_data.iloc[i].name)
    pred = helper.predict(label, txt, model)
