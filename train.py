from __future__ import absolute_import, division, print_function, unicode_literals

from optparse import OptionParser
from sys import platform
import params
import helper
from parser import parse

C = params.Config()

path = ''
if platform == "linux" or platform == "linux2":
    path = str("/")
elif platform == "darwin":
    path = str("./")
elif platform == "win32":
    path = str("../")
else:
    NameError("Error: System type not defined. Please check platform name " + platform)

parser = OptionParser()

parser.add_option("-p", "--path", dest="train_path",
                  help="Path to training data.")
parser.add_option("-e", "--epochs", dest="epochs",
                  help="Number of epochs.", default=5)
parser.add_option("-b", "--batch_size", dest="batch_size",
                  help="Number of batches.", default=128)
parser.add_option("-a", "--activation", dest="activation",
                  help="Type of activation function, list of possible in params file.", default="sigmoid")
parser.add_option("--checker", dest="checker", help="Checker over activation functions", default=False)

(options, args) = parser.parse_args()
if not options.train_path:  # if filename is not given
    parser.error(
        'Error: path to training data must be specified. Pass --path to command line')
df = parse(path + options.train_path)
X, X_train, Y_train, X_test, Y_test = helper.tokenize_data(df)

if options.checker:
    print("Running activation checker")
    C.EPOCHS_PARAM = 2
    options.activation = helper.check_activations(path, X, X_train, Y_train, X_test, Y_test)

C.EPOCHS_PARAM = int(options.epochs)
C.BATCH_PARAM = int(options.batch_size)

helper.run_training(options.activation, path, X, X_train, Y_train, X_test, Y_test)
